# DotaWebsiteReact
![Dota Image](https://cdn.cloudflare.steamstatic.com/apps/dota2/images/blog/play/dota_heroes.png)

This website will be able to view match statistics, display graphs of information pertaining to the match, and produce stats based on your games

## Landing Page
Provides a page to learn about dota and to supply a Match ID to use for displaying the view match page.

## View Match Page
Takes a Match ID and gets Dota match data.

## Workspace Installation Guide

## Node 
```
node --version
> v15.9.0
```

## Yarn   
Yarn is a package manager akin to NPM but has some performance enhancements and has a cleaner CLI. 
Windows users should use Chocolatey to install Yarn
```
yarn --version
> 1.22.5
```

## Install necessary JS packages
```
cd dota-website
yarn
```

## Run the web server
```
cd dota-website // (if not in it already)
yarn start
```

## Run the middleware server

First thing to do is to set up a tunnel to Elvis so the middleware can access the MySQL server.
Once that's done you'll have to configure src/backendRoutes.js to use the tunnel properly
```
cd dota-website // (if not in it already)
node server.js
```

## List of Dependencies
```
animate.css (for CSS animations)
axios (wraps fetches into Promises for performing GETs and POSTs with ease)
express (setting up routes for backend server)
moment (makes handling dates easier)
mysql2 (connector for MySQL server)
react (duh)
redux (for easier state-management)
react-redux (so react and redux play nice)
semantic-ui-css and semantic-ui-react (pre-made React elements like Input, Grid, and Button make it easier to create pleasant UX)
```
