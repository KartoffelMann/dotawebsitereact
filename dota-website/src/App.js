import React, { Component } from 'react';
import { Router, Switch, Route } from 'react-router-dom'
import history from './history.js'


import { connect } from "react-redux";

import 'animate.css'


import HomePage from './components/pages/HomePage'
import MatchViewPage from './components/pages/MatchViewPage'
import Header from './components/pages/Header'
import RecentMatchesPage from './components/pages/RecentMatchesPage'
import PlayerProfilePage from './components/pages/PlayerProfilePage'
import PlayerSearch from './components/pages/PlayerSearch'

class App extends Component {
  
  render() {
    return (
      <Router history={history}>
        <div className="App">
          <Header/>
          <Switch>
            <Route exact path="/" component={HomePage}/>
            <Route path="/match/:matchID" component={MatchViewPage}/>
            <Route path="/matches" component={RecentMatchesPage}/>
            <Route path="/players/:playerID" component={PlayerProfilePage}/>
            <Route path="/search/:personaName" component={PlayerSearch}/>
          </Switch>
        </div>
      </Router>
    )
  }
}

const mapStateToProps = state => ({
});


export default connect(mapStateToProps)(App);
