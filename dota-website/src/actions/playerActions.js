import axios from 'axios'

export const playerConstants = {
    START:      'PLAYER_GET_START',
    SUCCESS:    'PLAYER_GET_SUCCESS',
    ERROR:      'PLAYER_GET_ERROR',
    START_PROFILE : 'PLAYER_GET_FULL_START',
    SUCCESS_PROFILE : 'PLAYER_GET_FULL_SUCCESS',
    ERROR_PROFILE : 'PLAYER_GET_FULL_ERROR'
};

export const start = () => ({
    type: playerConstants.START 
})

export const startProfile = () => ({
    type: playerConstants.START_PROFILE 
})

export const success = (data) => ({
    type: playerConstants.SUCCESS, 
    payload: data 
})

export const fullProfileSuccess = (data) => ({
    type: playerConstants.SUCCESS_PROFILE,
    payload: data
})

export const error = (message) => ({
    type: playerConstants.ERROR, 
    payload: message
})

export const errorProfile = (error) => ({
    type: playerConstants.ERROR_PROFILE, 
    payload: error
})

const playerDataInstance = axios.create({
    timeout: 10000,
    headers: { 'Content-Type' : 'application/json' },
})

export function getPlayerProfile(account_id) {

    return dispatch => {
        dispatch(startProfile()) 
        let batchRequest = []
			
        batchRequest.push(playerDataInstance.get(`https://api.opendota.com/api/players/${account_id}`))
        batchRequest.push(playerDataInstance.get(`https://api.opendota.com/api/players/${account_id}/wl`))
        batchRequest.push(playerDataInstance.get(`https://api.opendota.com/api/players/${account_id}/recentMatches`))
			
        Promise.allSettled(batchRequest)
            .then(function (results) {

                let resultData = {
                    profileData : results[0].value.data,
                    wl          : results[1].value.data,
                    recentMatches: results[2].value.data
                }

                console.log(resultData)

                dispatch(fullProfileSuccess(resultData))
            })
            .catch(function (error) {
                dispatch(errorProfile(error))
            })
    }
}

export function getPlayerSearch(queryString) {

    return dispatch => {
        dispatch(start())
        axios({
            url: `https://api.opendota.com/api/search?q=${queryString}`,
            method: "get",
            headers: {
                "Content-Type" : 'application/json'
            },
        })
        .then(function(response) {
            dispatch(success(response))
        })
        .catch(function(error) {
            dispatch(error(error));
        });

    }
}

export function getPlayerWL(account_id) {
    
    return dispatch => {
        dispatch(start()) 
        axios({
            url: `https://api.opendota.com/api/players/${account_id}/wl`,
            method: "get",
            headers: {
                "Content-Type" : 'application/json'
            },
        })
        .then(function(response) {
            dispatch(success(response))
        })
        .catch(function(error) {
            dispatch(error(error));
        });
    }
}

export function getPlayerMatchDataQuery(account_id, limit, offset, win, game_mode, lane_role, hero_id, is_radiant) {

    return dispatch => {
        dispatch(start())
        axios({
            url: `https://api.opendota.com/api/players/${account_id}/matches`,
            method: "get",
            headers: {
                "Content-Type" : 'application/json'
            },
            params: {
                limit: limit,
                offset: offset,
                win: win,
                game_mode: game_mode,
                lane_role: lane_role,
                hero_id: hero_id,
                is_radiant: is_radiant
            }
        })
        .then(function(response) {
            dispatch(success(response))
        })
        .catch(function(error) {
            dispatch(error(error));
        });
    }
}

export function getPlayerHeroDataQuery(account_id, limit, offset, win, game_mode, lane_role, hero_id, is_radiant) {

    return dispatch => {
        dispatch(start())
        axios({
            url: `https://api.opendota.com/api/players/${account_id}/heroes`,
            method: "get",
            headers: {
                "Content-Type" : 'application/json'
            },
            params: {
                limit: limit,
                offset: offset,
                win: win,
                game_mode: game_mode,
                lane_role: lane_role,
                hero_id: hero_id,
                is_radiant: is_radiant
            }
        })
        .then(function(response) {
            dispatch(success(response))
        })
        .catch(function(error) {
            dispatch(error(error));
        });
    }
}