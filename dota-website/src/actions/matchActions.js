import axios from 'axios'

export const matchConstants = {
    START:      'MATCH_GET_START',
    SUCCESS:    'MATCH_GET_SUCCESS',
    ERROR:      'MATCH_GET_ERROR'
};

export const start = () => ({
    type: matchConstants.START 
})

export const success = (data) => ({
    type: matchConstants.SUCCESS, 
    payload: data 
})

export const error = (message) => ({
    type: matchConstants.ERROR, 
    payload: message
})

export function getMatchData(matchID) {

    return dispatch => {
        axios({
            baseURL : "http://localhost:4000",
            url: "/api/check_matchID",
            method: "post",
            headers: {
                "Content-Type" : 'application/json'
            },
            data: {
                matchID : matchID
            }
        })
        .then(function(response) {
            console.log(response)
            dispatch(success(response))
        });
    }
}