import axios from 'axios'

export const recentMatchConstants = {
    START:      'RECENT_MATCHES_GET_START',
    SUCCESS:    'RECENT_MATCHES_GET_SUCCESS',
    ERROR:      'RECENT_MATCHES_GET_ERROR'
};

export const start = () => ({
    type: recentMatchConstants.START 
})

export const success = (data) => ({
    type: recentMatchConstants.SUCCESS, 
    payload: data 
})

export const error = (message) => ({
    type: recentMatchConstants.ERROR, 
    payload: message
})

export function getRecentMatches() {

    return dispatch => {

        dispatch(start())

        axios({
            url: "https://api.opendota.com/api/publicMatches",
            method: "get",
            headers: {
                "Content-Type" : 'application/json'
            },
        })
        .then(function(response) {
            dispatch(success(response))
        })
        .catch(function(error) {
            dispatch(error(error))
        });
    }
}

export function getRecentMatchesFromPlayer(account_id) {

    return dispatch => {
        dispatch(start())
        axios({
            url: `https://api.opendota.com/api/players/${account_id}/recentMatches`,
            method: "get",
            headers: {
                "Content-Type" : 'application/json'
            },
        })
        .then(function(response) {
            dispatch(success(response))
        })
        .catch(function(error) {
            dispatch(error(error));
        });
    }
}