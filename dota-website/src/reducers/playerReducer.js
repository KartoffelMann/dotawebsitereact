import { playerConstants } from '../actions/playerActions'

const initialState = {
    loading: true,
    response: null,
    error: null
}

export default function playerReducer(state = initialState, action) {
    switch(action.type) {
        case playerConstants.SUCCESS_PROFILE:
            return {
                ...state,
                type: 'player-success',
                response: action.payload,
                loading: false,
            } 

        case playerConstants.ERROR_PROFILE:
                return {
                    type: 'player-error',
                    error: action.payload.error,
                    loading: false
                };

        case playerConstants.START:
            return {
                ...state,
                type: 'player-start',
                loading: true,
            }

        case playerConstants.SUCCESS:
            return {
                ...state, 
                type: 'player-success',
                response: action.payload.data,
                loading: false
            }
        default: 
            return state;
    }
}