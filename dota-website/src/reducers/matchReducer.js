import { matchConstants } from '../actions/matchActions'

const initialState = {
    loading: true,
    response: null, 
    error: null,
}

export default function matchReducer(state = initialState, action) {
    switch(action.type) {
        case matchConstants.SUCCESS:
            return {
                ...state,
                type: 'match-success',
                response: action.payload.data,
                loading: false
            };

        case matchConstants.ERROR:
            return {
                type: 'match-error',
                error: action.payload.error,
                loading: false
            };
        default: 
            return state;
    }
}