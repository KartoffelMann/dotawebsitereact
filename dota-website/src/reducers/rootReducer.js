import { combineReducers } from 'redux'

import match from './matchReducer.js'
import recentMatches from './recentMatchReducer'
import player from './playerReducer'

export default combineReducers({
    match,
    recentMatches,
    player
})