import { recentMatchConstants } from '../actions/recentMatchActions'

const initialState = {
    loading: true,
    response: null, 
    error: null,
}

export default function recentMatchesReducer(state = initialState, action) {
    switch(action.type) {
        case recentMatchConstants.SUCCESS:
            return {
                ...state,
                type: 'match-success',
                response: action.payload.data,
                loading: false
            };

        case recentMatchConstants.ERROR:
            return {
                type: 'match-error',
                error: action.payload.error,
                loading: false
            };
        default: 
            return state;
    }
}