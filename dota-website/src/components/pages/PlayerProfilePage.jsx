import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Table, Image } from 'semantic-ui-react'

import PlayerProfileRecentMatchComponent from './PlayerProfileRecentMatchComponent'

// For debugging
//import playerProfile from '../static/json/profile.json'
//import playerWL from '../static/json/profile_wl.json'
//import recentMatches from '../static/json/profile_recentMatches.json'
import { getPlayerProfile } from '../../actions/playerActions'

class PlayerProfilePage extends Component {

    componentDidMount() {
        console.log(this)
        let account_id = this.props.match.params.playerID
        console.log(account_id)

        this.props.dispatch(getPlayerProfile(account_id))
    }

    returnRanks = (player_rank) => {
        if(player_rank === undefined) {
            return <p>Unknown</p> ;
        } else {
            let rankStr = String(player_rank)
            let ranks = {
                "1": "Herald",
                "2": "Guardian",
                "3": "Crusader",
                "4": "Archon",
                "5": "Legend",
                "6": "Ancient",
                "7": "Divine"
            };
            let badge = ranks[rankStr.charAt(0)]

            return <p>{badge} {rankStr.charAt(1)}</p>
        }
        
        
    }

    render() {

        const playerProfile = this.props.profileData;
        const playerWL = this.props.wl;
        const recentMatches = this.props.recentMatches;
        const isLoading = this.props.isLoading
        return(
            <div style={{margin: '50px 100px 0 100px'}}>
                { !isLoading &&
                    <div>
                        <Table inverted className="animate__animated animate__fadeIn">
                            <Table.Header>
                                <Table.Row>
                                    <Table.HeaderCell width={1}>Avatar</Table.HeaderCell>
                                    <Table.HeaderCell width={1}>Player Name</Table.HeaderCell>
                                    <Table.HeaderCell width={1}>Rank</Table.HeaderCell>
                                    <Table.HeaderCell width={1}>Win - Lose</Table.HeaderCell>
                                </Table.Row>
                            </Table.Header>

                            <Table.Body>
                                <Table.Row>
                                    <Table.Cell>
                                        <Image size="small" src={playerProfile.profile.avatarfull}/>
                                    </Table.Cell>
                                    <Table.Cell>
                                        <p>{playerProfile.profile.personaname}</p>
                                    </Table.Cell>
                                    <Table.Cell>
                                        {this.returnRanks(playerProfile.rank_tier)}
                                    </Table.Cell>
                                    <Table.Cell>
                                        <span style={{color: 'green'}}>{playerWL.win}</span>
                                        <span> - </span>
                                        <span style={{color: 'red'}}>{playerWL.lose}</span >
                                    </Table.Cell>
                                </Table.Row>
                            </Table.Body>
                        </Table>
                        <div className="animate__animated animate__fadeIn animate__delay-1s">
                            <h2>Recent Matches</h2>
                            <Table inverted>
                                <Table.Header>
                                    <Table.Row textAlign="center">
                                        <Table.HeaderCell width={1}>Hero</Table.HeaderCell>
                                        <Table.HeaderCell width={1}>Result</Table.HeaderCell>
                                        <Table.HeaderCell width={1}>Type</Table.HeaderCell>
                                        <Table.HeaderCell width={1}>Duration</Table.HeaderCell>
                                        <Table.HeaderCell width={1}>KDA</Table.HeaderCell>
                                    </Table.Row>
                                </Table.Header>


                                <Table.Body>
                                    { Object.keys(recentMatches).map(key => {
                                        return (
                                            <PlayerProfileRecentMatchComponent history={this.props.history} key={key} data={recentMatches[key]}/>
                                        )
                                    })}
                                </Table.Body>
                            </Table>
                        </div>
                        

                        
                    </div>
                }
                
            </div>
        )
    }
}

const mapStateToProps = state => ({
    wl: state.player.response?.wl,
    profileData: state.player.response?.profileData,
    recentMatches: state.player.response?.recentMatches,
    isLoading: state.player.loading
});


export default connect(mapStateToProps)(PlayerProfilePage);