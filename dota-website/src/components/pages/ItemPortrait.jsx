import React, { Component } from 'react'
import itemList from '../static/json/dota_items.json'

// Makes a img tag and uses the hero list to get the correct image of the hero

class ItemPortrait extends Component {

    constructor(props) {
        super(props);

        this.state = {
            itemID : props.itemID
        }
    }

    render() {
        let url = 'https://cdn.cloudflare.steamstatic.com/apps/dota2/images/dota_react/items/';

        if(this.state.itemID !== 0) {
            url += (itemList[`${this.state.itemID}`] + ".png")

            return (
                <div>
                    <img style={{width: '75%', height: 'auto'}} src={url} title={itemList[this.state.itemID]} alt={itemList[this.state.itemID]}/>
                </div>
            )
        } else {
            return (<div></div>)
        }
        
    }
}

export default ItemPortrait;