import React, { Component } from 'react'
import RecentMatchesComponent from './RecentMatchesComponent'

import { connect } from 'react-redux'
import { Table } from 'semantic-ui-react';

// For debugging
//import recentMatches from '../static/json/recentMatches.json'
import { getRecentMatches } from '../../actions/recentMatchActions';

class RecentMatchesPage extends Component {

    componentDidMount() {
        this.props.dispatch(getRecentMatches())
    }

    render() {

        // const renderBodyRow = (data, i) => {
        //     console.log(data);
        //     console.log(i)
        // }
        return (
            <div style={{margin: '0 50px 0 50px'}}>
                <Table inverted celled className="animate__animated animate__fadeIn animate__delay-1s">
                    <Table.Header>
                        <Table.Row>
                            <Table.HeaderCell>Match ID</Table.HeaderCell>
                            <Table.HeaderCell>Game Mode</Table.HeaderCell>
                            <Table.HeaderCell>Result</Table.HeaderCell>
                            <Table.HeaderCell>Duration</Table.HeaderCell>
                            <Table.HeaderCell>Radiant Team</Table.HeaderCell>
                            <Table.HeaderCell>Dire Team</Table.HeaderCell>
                        </Table.Row>
                    </Table.Header>

                    {!this.props.matches.loading &&
                    <Table.Body>
                        { Object.keys(this.props.matches.response).map(key => {
                            return (
                                <RecentMatchesComponent history={this.props.history} key={key} data={this.props.matches.response[key]}/>
                            )
                        })}
                    </Table.Body>
                    
                    }
                  </Table>  
            </div>
            
        )
    }
    

    
}

const mapStateToProps = state => ({
    matches: state.recentMatches
});


export default connect(mapStateToProps)(RecentMatchesPage);