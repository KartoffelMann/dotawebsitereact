import React, { Component } from 'react'

import { Table } from 'semantic-ui-react'
import { connect } from 'react-redux'
import { getMatchData } from '../../actions/matchActions'

import '../static/css/HomePage.css'
import '../static/css/MatchViewPage.css'

import HeroPortrait from './HeroPortrait'
import ItemPortrait from './ItemPortrait'

class MatchViewPage extends Component {

    constructor(props) {
        super(props);
        
        this.state = {
            matchID: this.props.match.params.matchID,
        }
    }


    componentDidMount() {
        this.props.dispatch(getMatchData(this.state.matchID))
        
    }

    returnSkillString(num) {
        switch(num) {
            case 1: 
                return "Normal Skill"
            case 2: 
                return "High Skill"
            case 3: 
                return "Very High Skill"
            default:
                return "Normal Skill"
        }
    }


    /**
     * Takes an integer value of seconds in the match, returns a formatted version MM:SS
     * @param {*} duration 
     * @returns Formatted String of match duration
     */
    returnDurationString(duration) {
        let minutes = duration / 60;

        let seconds = duration % 60;
        if(seconds < 10) {
            seconds = `0${seconds}`
        }

        return `${minutes | 0}:${seconds}`
    }

    returnMatchType(num) {
        switch(num) {
            case 0: {
                return "Unknown Game Mode"
            }
            case 1: {
                return "All Pick"
            }
            case 2: {
                return "Captains Mode"
            }
            case 3: {
                return "Random Draft"
            }
            case 4: {
                return "Single Draft"
            }
            case 5: {
                return "All Random"
            }
            case 6: {
                return "Intro"
            }
            case 7: {
                return "Reverse Captains Mode"
            }
            case 8: {
                return "Diretide"
            }
            case 9: {
                return "Greeviling"
            }
            case 10: {
                return "Tutorial"
            }
            case 11: {
                return "Mid Only"
            }
            case 12: {
                return "Least Played Heroes"
            }
            case 13: {
                return "Limited Heroes"
            }
            case 14: {
                return "Compendium Matchmaking"
            }
            case 15: {
                return "Custom Game"
            }
            case 16: {
                return "Captains Draft"
            }
            case 17: {
                return "Balanced Draft"
            }
            case 18: {
                return "Ability Draft"
            }
            case 19: {
                return "Event"
            }
            case 20: {
                return "All Random Deathmatch"
            }
            case 21: {
                return "1v1 Mid"
            }
            case 22: {
                return "All Pick"
            }
            case 23: {
                return "Turbo"
            }
            case 24: {
                return "Mutation"
            }
            default: {
                return "Unspecified"
            }
        }
    }

    // https://github.com/SteamDatabase/GameTracking-Dota2/blob/master/game/dota/pak01_dir/scripts/regions.txt
    returnRegionString(num) {
        switch(num) {
            case 1: return "US West"
            case 2: return "US East"
            case 3: return "Europe"
            case 5: return "Singapore"
            case 6: return "Dubai"
            case 12: return "Perfect World (China)"
            case 17: return "Perfect World (Guangdong)"
            case 18: return "Perfect World (Zhejiang)"
            case 20: return "Perfect World (Guangdong)"
            case 13: return "Perfect World (Unicom)"
            case 25: return "Perfect World (Unicom Tianjin)"
            case 8: return "Stockholm"
            case 10: return "Brazil"
            case 9: return "Austria"
            case 7: return "Australia"
            case 11: return "South Africa"
            case 14: return "Chile"
            case 15: return "Peru"
            case 16: return "India"
            case 19: return "Japan"
            case 37: return "Taiwan #1"
            default: return "Unknown"
        }
    }

    // https://github.com/odota/dotaconstants/blob/master/json/lobby_type.json
    returnLobbyType(num) {
        switch(num) {
            case 0: return "Normal"
            case 1: return "Practice"
            case 2: return "Tournament"
            case 3: return "Tutorial"
            case 4: return "Co-op Bots"
            case 5: return "Ranked Team Matchmaking"
            case 6: return "Ranked Solo Matchmaking"
            case 7: return "Ranked"
            case 8: return "1v1 Mid"
            case 9: return "Battle Cup"
            default: return "Unknown"
        }
    }

    returnWinnerElement(radiant_win) {
        if(radiant_win === true) {
            return <h2 style={{color: 'green'}}>RADIANT VICTORY</h2>;
        } else {
            return <h2 style={{color: 'red'}}>DIRE VICTORY</h2>;
        }
    }

    personaNameResolver(name, account_id) {
        if (name === undefined) {
            return "Anonymous";
        } else {
            return <a href={`/players/${account_id}`}>{name}</a>;
        }
    }

    render() {
        console.log(this);
        
        

        const matchHeader = {
            marginLeft: '50px',
            marginRight: '50px',
            paspaningLeft: '25px',
            display: 'block',
            boxSizing: 'border-box'
        }

        const verticalLine = {
            borderLeft : '2px solid white',
            height: '50px',
            marginRight: '10px'
        }

        const itemTableCell = {
            display: 'flex',
            verticalAlign: 'mispanle'
        }

        const matchData = this.props.matchData?.matchData
            return (
                
                <div>
                { !this.props.isLoading &&
                    <div>
                    <div className="animate__animated animate__fadeIn">
                        <h1 style={{textAlign: 'center', verticalAlign: 'bottom'}}>Match {matchData.match_id}</h1>
                    </div>

                    <div style={matchHeader} className="animate__animated animate__fadeIn">
                        <div style={{textAlign: 'right', marginRight: '100px'}}>
    
                            <div>
                                <span>Skill Bracket - </span>
                                <span>{this.returnSkillString(matchData.skill)} </span>
                            </div>
    
                            <div>
                                <span>Lobby Type - </span>
                                <span>{this.returnLobbyType(matchData.lobby_type)}</span>
                            </div>
    
                            <div>
                                <span>Game Mode - </span>
                                <span>{this.returnMatchType(matchData.game_mode)}</span>
                            </div>
    
                            <div>
                                <span>Region - </span>
                                <span>{this.returnRegionString(matchData.region)}</span>
                                
                            </div>
    
                            <div>
                                <span>Duration - </span>
                                <span>{this.returnDurationString(matchData.duration)}</span>
                            </div>
    
                        </div>
                    </div>
    
                    <div style={{ textAlign: 'center', marginTop: '50px'}} className="animate__animated animate__fadeIn">
                        {this.returnWinnerElement(matchData.radiant_win)}
                        <div>
                            <span style={{color: 'green', marginRight: '10px', fontSize: '24px'}}>{matchData.radiant_score}</span> 
                            <span style={{marginRight: '10px', fontSize: '16px'}}>{this.returnDurationString(matchData.duration)}</span> 
                            <span style={{color: 'red', fontSize: '24px'}}>{matchData.dire_score}</span>
                        </div>
                    </div>
    
                    <br></br>
    
                    <div style={{textAlign: 'center'}} className="animate__animated animate__fadeIn">
                        <h2 style={{color: 'green', marginTop: '20px'}}>THE RADIANT</h2>
    
                        <div className="container">
                            <Table inverted style={{margin: '0 100px 0 100px'}} /* style={tableStyle} */>
                                <Table.Header  /*style={rowStyle} */>
                                    <Table.Row>
                                        <Table.HeaderCell width={2}>Hero</Table.HeaderCell>
                                        <Table.HeaderCell>Player</Table.HeaderCell>
                                        <Table.HeaderCell>K</Table.HeaderCell>
                                        <Table.HeaderCell>D</Table.HeaderCell>
                                        <Table.HeaderCell>A</Table.HeaderCell>
                                        <Table.HeaderCell>Gold</Table.HeaderCell>
                                        <Table.HeaderCell>Net Worth</Table.HeaderCell>
                                        <Table.HeaderCell>LH / DN</Table.HeaderCell>
                                        <Table.HeaderCell>GPM / XPM</Table.HeaderCell>
                                        <Table.HeaderCell>DMG</Table.HeaderCell>
                                        <Table.HeaderCell>Heal</Table.HeaderCell>
                                        <Table.HeaderCell>BLD</Table.HeaderCell>
                                        <Table.HeaderCell width={5}>Items</Table.HeaderCell>
                                    </Table.Row>
                                </Table.Header>

                                <Table.Body>
                                    <Table.Row>

                                        <Table.Cell>
                                            <HeroPortrait heroID={matchData.players[0].hero_id} />
                                        </Table.Cell>

                                        <Table.Cell>
                                            {this.personaNameResolver(matchData.players[0].personaname, matchData.players[0].account_id)}
                                        </Table.Cell>

                                        <Table.Cell>{matchData.players[0].kills}</Table.Cell>
                                        <Table.Cell>{matchData.players[0].deaths}</Table.Cell>
                                        <Table.Cell>{matchData.players[0].assists}</Table.Cell>
                                        <Table.Cell>{matchData.players[0].gold}</Table.Cell>
                                        <Table.Cell>{matchData.players[0].net_worth}</Table.Cell>
                                        <Table.Cell>{matchData.players[0].last_hits} / {matchData.players[0].denies}</Table.Cell>
                                        <Table.Cell>{matchData.players[0].gold_per_min} / {matchData.players[0].xp_per_min}</Table.Cell>
                                        <Table.Cell>{matchData.players[0].hero_damage}</Table.Cell>
                                        <Table.Cell>{matchData.players[0].hero_healing}</Table.Cell>
                                        <Table.Cell>{matchData.players[0].tower_damage}</Table.Cell>

                                        <Table.Cell>
                                            <div style={itemTableCell}>
                                                <ItemPortrait itemID = {matchData.players[0].item_0}/>
                                                <ItemPortrait itemID = {matchData.players[0].item_1}/>
                                                <ItemPortrait itemID = {matchData.players[0].item_2}/>
                                                <ItemPortrait itemID = {matchData.players[0].item_3}/>
                                                <ItemPortrait itemID = {matchData.players[0].item_4}/>
                                                <ItemPortrait itemID = {matchData.players[0].item_5}/>
                                                <div style={verticalLine}></div>
                                                <ItemPortrait itemID = {matchData.players[0].backpack_0}/>
                                                <ItemPortrait itemID = {matchData.players[0].backpack_1}/>
                                                <ItemPortrait itemID = {matchData.players[0].backpack_2}/>
                                                <div style={verticalLine}></div>
                                                <ItemPortrait itemID = {matchData.players[0].item_neutral}/>
                                            </div>
                                        </Table.Cell>

                                    </Table.Row>

                                    <Table.Row>

                                        <Table.Cell>
                                            <HeroPortrait heroID={matchData.players[1].hero_id} />
                                        </Table.Cell>

                                        <Table.Cell>
                                            {this.personaNameResolver(matchData.players[1].personaname, matchData.players[1].account_id)}
                                        </Table.Cell>

                                        <Table.Cell>{matchData.players[1].kills}</Table.Cell>
                                        <Table.Cell>{matchData.players[1].deaths}</Table.Cell>
                                        <Table.Cell>{matchData.players[1].assists}</Table.Cell>
                                        <Table.Cell>{matchData.players[1].gold}</Table.Cell>
                                        <Table.Cell>{matchData.players[1].net_worth}</Table.Cell>
                                        <Table.Cell>{matchData.players[1].last_hits} / {matchData.players[1].denies}</Table.Cell>
                                        <Table.Cell>{matchData.players[1].gold_per_min} / {matchData.players[1].xp_per_min}</Table.Cell>
                                        <Table.Cell>{matchData.players[1].hero_damage}</Table.Cell>
                                        <Table.Cell>{matchData.players[1].hero_healing}</Table.Cell>
                                        <Table.Cell>{matchData.players[1].tower_damage}</Table.Cell>

                                        <Table.Cell>
                                            <div style={itemTableCell}>
                                                <ItemPortrait itemID = {matchData.players[1].item_0}/>
                                                <ItemPortrait itemID = {matchData.players[1].item_1}/>
                                                <ItemPortrait itemID = {matchData.players[1].item_2}/>
                                                <ItemPortrait itemID = {matchData.players[1].item_3}/>
                                                <ItemPortrait itemID = {matchData.players[1].item_4}/>
                                                <ItemPortrait itemID = {matchData.players[1].item_5}/>
                                                <div style={verticalLine}></div>
                                                <ItemPortrait itemID = {matchData.players[1].backpack_0}/>
                                                <ItemPortrait itemID = {matchData.players[1].backpack_1}/>
                                                <ItemPortrait itemID = {matchData.players[1].backpack_2}/>
                                                <div style={verticalLine}></div>
                                                <ItemPortrait itemID = {matchData.players[1].item_neutral}/>
                                            </div>
                                        </Table.Cell>

                                    </Table.Row>

                                    <Table.Row>

                                        <Table.Cell>
                                            <HeroPortrait heroID={matchData.players[2].hero_id} />
                                        </Table.Cell>

                                        <Table.Cell>
                                            {this.personaNameResolver(matchData.players[2].personaname, matchData.players[2].account_id)}
                                        </Table.Cell>

                                        <Table.Cell>{matchData.players[2].kills}</Table.Cell>
                                        <Table.Cell>{matchData.players[2].deaths}</Table.Cell>
                                        <Table.Cell>{matchData.players[2].assists}</Table.Cell>
                                        <Table.Cell>{matchData.players[2].gold}</Table.Cell>
                                        <Table.Cell>{matchData.players[2].net_worth}</Table.Cell>
                                        <Table.Cell>{matchData.players[2].last_hits} / {matchData.players[2].denies}</Table.Cell>
                                        <Table.Cell>{matchData.players[2].gold_per_min} / {matchData.players[2].xp_per_min}</Table.Cell>
                                        <Table.Cell>{matchData.players[2].hero_damage}</Table.Cell>
                                        <Table.Cell>{matchData.players[2].hero_healing}</Table.Cell>
                                        <Table.Cell>{matchData.players[2].tower_damage}</Table.Cell>

                                        <Table.Cell>
                                            <div style={itemTableCell}>
                                                <ItemPortrait itemID = {matchData.players[2].item_0}/>
                                                <ItemPortrait itemID = {matchData.players[2].item_1}/>
                                                <ItemPortrait itemID = {matchData.players[2].item_2}/>
                                                <ItemPortrait itemID = {matchData.players[2].item_3}/>
                                                <ItemPortrait itemID = {matchData.players[2].item_4}/>
                                                <ItemPortrait itemID = {matchData.players[2].item_5}/>
                                                <div style={verticalLine}></div>
                                                <ItemPortrait itemID = {matchData.players[2].backpack_0}/>
                                                <ItemPortrait itemID = {matchData.players[2].backpack_1}/>
                                                <ItemPortrait itemID = {matchData.players[2].backpack_2}/>
                                                <div style={verticalLine}></div>
                                                <ItemPortrait itemID = {matchData.players[2].item_neutral}/>
                                            </div>
                                        </Table.Cell>

                                    </Table.Row>

                                    <Table.Row>

                                        <Table.Cell>
                                            <HeroPortrait heroID={matchData.players[3].hero_id} />
                                        </Table.Cell>

                                        <Table.Cell>
                                            {this.personaNameResolver(matchData.players[3].personaname, matchData.players[3].account_id)}
                                        </Table.Cell>

                                        <Table.Cell>{matchData.players[3].kills}</Table.Cell>
                                        <Table.Cell>{matchData.players[3].deaths}</Table.Cell>
                                        <Table.Cell>{matchData.players[3].assists}</Table.Cell>
                                        <Table.Cell>{matchData.players[3].gold}</Table.Cell>
                                        <Table.Cell>{matchData.players[3].net_worth}</Table.Cell>
                                        <Table.Cell>{matchData.players[3].last_hits} / {matchData.players[3].denies}</Table.Cell>
                                        <Table.Cell>{matchData.players[3].gold_per_min} / {matchData.players[3].xp_per_min}</Table.Cell>
                                        <Table.Cell>{matchData.players[3].hero_damage}</Table.Cell>
                                        <Table.Cell>{matchData.players[3].hero_healing}</Table.Cell>
                                        <Table.Cell>{matchData.players[3].tower_damage}</Table.Cell>

                                        <Table.Cell>
                                            <div style={itemTableCell}>
                                                <ItemPortrait itemID = {matchData.players[3].item_0}/>
                                                <ItemPortrait itemID = {matchData.players[3].item_1}/>
                                                <ItemPortrait itemID = {matchData.players[3].item_2}/>
                                                <ItemPortrait itemID = {matchData.players[3].item_3}/>
                                                <ItemPortrait itemID = {matchData.players[3].item_4}/>
                                                <ItemPortrait itemID = {matchData.players[3].item_5}/>
                                                <div style={verticalLine}></div>
                                                <ItemPortrait itemID = {matchData.players[3].backpack_0}/>
                                                <ItemPortrait itemID = {matchData.players[3].backpack_1}/>
                                                <ItemPortrait itemID = {matchData.players[3].backpack_2}/>
                                                <div style={verticalLine}></div>
                                                <ItemPortrait itemID = {matchData.players[3].item_neutral}/>
                                            </div>
                                        </Table.Cell>

                                    </Table.Row>

                                    <Table.Row>

                                        <Table.Cell>
                                            <HeroPortrait heroID={matchData.players[4].hero_id} />
                                        </Table.Cell>

                                        <Table.Cell>
                                            {this.personaNameResolver(matchData.players[4].personaname, matchData.players[4].account_id)}
                                        </Table.Cell>

                                        <Table.Cell>{matchData.players[4].kills}</Table.Cell>
                                        <Table.Cell>{matchData.players[4].deaths}</Table.Cell>
                                        <Table.Cell>{matchData.players[4].assists}</Table.Cell>
                                        <Table.Cell>{matchData.players[4].gold}</Table.Cell>
                                        <Table.Cell>{matchData.players[4].net_worth}</Table.Cell>
                                        <Table.Cell>{matchData.players[4].last_hits} / {matchData.players[4].denies}</Table.Cell>
                                        <Table.Cell>{matchData.players[4].gold_per_min} / {matchData.players[4].xp_per_min}</Table.Cell>
                                        <Table.Cell>{matchData.players[4].hero_damage}</Table.Cell>
                                        <Table.Cell>{matchData.players[4].hero_healing}</Table.Cell>
                                        <Table.Cell>{matchData.players[4].tower_damage}</Table.Cell>

                                        <Table.Cell>
                                            <div style={itemTableCell}>
                                                <ItemPortrait itemID = {matchData.players[4].item_0}/>
                                                <ItemPortrait itemID = {matchData.players[4].item_1}/>
                                                <ItemPortrait itemID = {matchData.players[4].item_2}/>
                                                <ItemPortrait itemID = {matchData.players[4].item_3}/>
                                                <ItemPortrait itemID = {matchData.players[4].item_4}/>
                                                <ItemPortrait itemID = {matchData.players[4].item_5}/>
                                                <div style={verticalLine}></div>
                                                <ItemPortrait itemID = {matchData.players[4].backpack_0}/>
                                                <ItemPortrait itemID = {matchData.players[4].backpack_1}/>
                                                <ItemPortrait itemID = {matchData.players[4].backpack_2}/>
                                                <div style={verticalLine}></div>
                                                <ItemPortrait itemID = {matchData.players[4].item_neutral}/>
                                            </div>
                                        </Table.Cell>

                                    </Table.Row>
                                </Table.Body>
                            </Table>
                        </div>
                        
    
                    </div>
    
                    <br></br>
    
                    <div style={{textAlign: 'center'}}>
                        <h2 style={{color: 'red'}}>THE DIRE</h2>
    
                        <div className="container">
                            <Table inverted style={{margin: '0 100px 50px 100px'}} /* style={tableStyle} */>
                                <Table.Header  /*style={rowStyle} */>
                                    <Table.Row>
                                        <Table.HeaderCell width={2}>Hero</Table.HeaderCell>
                                        <Table.HeaderCell>Player</Table.HeaderCell>
                                        <Table.HeaderCell>K</Table.HeaderCell>
                                        <Table.HeaderCell>D</Table.HeaderCell>
                                        <Table.HeaderCell>A</Table.HeaderCell>
                                        <Table.HeaderCell>Gold</Table.HeaderCell>
                                        <Table.HeaderCell>Net Worth</Table.HeaderCell>
                                        <Table.HeaderCell>LH / DN</Table.HeaderCell>
                                        <Table.HeaderCell>GPM / XPM</Table.HeaderCell>
                                        <Table.HeaderCell>DMG</Table.HeaderCell>
                                        <Table.HeaderCell>Heal</Table.HeaderCell>
                                        <Table.HeaderCell>BLD</Table.HeaderCell>
                                        <Table.HeaderCell width={5}>Items</Table.HeaderCell>
                                    </Table.Row>
                                </Table.Header>

                                <Table.Body>
                                    <Table.Row>

                                        <Table.Cell>
                                            <HeroPortrait heroID={matchData.players[5].hero_id} />
                                        </Table.Cell>

                                        <Table.Cell>
                                            {this.personaNameResolver(matchData.players[5].personaname, matchData.players[5].account_id)}
                                        </Table.Cell>

                                        <Table.Cell>{matchData.players[5].kills}</Table.Cell>
                                        <Table.Cell>{matchData.players[5].deaths}</Table.Cell>
                                        <Table.Cell>{matchData.players[5].assists}</Table.Cell>
                                        <Table.Cell>{matchData.players[5].gold}</Table.Cell>
                                        <Table.Cell>{matchData.players[5].net_worth}</Table.Cell>
                                        <Table.Cell>{matchData.players[5].last_hits} / {matchData.players[5].denies}</Table.Cell>
                                        <Table.Cell>{matchData.players[5].gold_per_min} / {matchData.players[5].xp_per_min}</Table.Cell>
                                        <Table.Cell>{matchData.players[5].hero_damage}</Table.Cell>
                                        <Table.Cell>{matchData.players[5].hero_healing}</Table.Cell>
                                        <Table.Cell>{matchData.players[5].tower_damage}</Table.Cell>

                                        <Table.Cell>
                                            <div style={itemTableCell}>
                                                <ItemPortrait itemID = {matchData.players[5].item_0}/>
                                                <ItemPortrait itemID = {matchData.players[5].item_1}/>
                                                <ItemPortrait itemID = {matchData.players[5].item_2}/>
                                                <ItemPortrait itemID = {matchData.players[5].item_3}/>
                                                <ItemPortrait itemID = {matchData.players[5].item_4}/>
                                                <ItemPortrait itemID = {matchData.players[5].item_5}/>
                                                <div style={verticalLine}></div>
                                                <ItemPortrait itemID = {matchData.players[5].backpack_0}/>
                                                <ItemPortrait itemID = {matchData.players[5].backpack_1}/>
                                                <ItemPortrait itemID = {matchData.players[5].backpack_2}/>
                                                <div style={verticalLine}></div>
                                                <ItemPortrait itemID = {matchData.players[5].item_neutral}/>
                                            </div>
                                        </Table.Cell>

                                    </Table.Row>

                                    <Table.Row>

                                        <Table.Cell>
                                            <HeroPortrait heroID={matchData.players[6].hero_id} />
                                        </Table.Cell>

                                        <Table.Cell>
                                            {this.personaNameResolver(matchData.players[6].personaname, matchData.players[6].account_id)}
                                        </Table.Cell>

                                        <Table.Cell>{matchData.players[6].kills}</Table.Cell>
                                        <Table.Cell>{matchData.players[6].deaths}</Table.Cell>
                                        <Table.Cell>{matchData.players[6].assists}</Table.Cell>
                                        <Table.Cell>{matchData.players[6].gold}</Table.Cell>
                                        <Table.Cell>{matchData.players[6].net_worth}</Table.Cell>
                                        <Table.Cell>{matchData.players[6].last_hits} / {matchData.players[6].denies}</Table.Cell>
                                        <Table.Cell>{matchData.players[6].gold_per_min} / {matchData.players[6].xp_per_min}</Table.Cell>
                                        <Table.Cell>{matchData.players[6].hero_damage}</Table.Cell>
                                        <Table.Cell>{matchData.players[6].hero_healing}</Table.Cell>
                                        <Table.Cell>{matchData.players[6].tower_damage}</Table.Cell>

                                        <Table.Cell>
                                            <div style={itemTableCell}>
                                                <ItemPortrait itemID = {matchData.players[6].item_0}/>
                                                <ItemPortrait itemID = {matchData.players[6].item_1}/>
                                                <ItemPortrait itemID = {matchData.players[6].item_2}/>
                                                <ItemPortrait itemID = {matchData.players[6].item_3}/>
                                                <ItemPortrait itemID = {matchData.players[6].item_4}/>
                                                <ItemPortrait itemID = {matchData.players[6].item_5}/>
                                                <div style={verticalLine}></div>
                                                <ItemPortrait itemID = {matchData.players[6].backpack_0}/>
                                                <ItemPortrait itemID = {matchData.players[6].backpack_1}/>
                                                <ItemPortrait itemID = {matchData.players[6].backpack_2}/>
                                                <div style={verticalLine}></div>
                                                <ItemPortrait itemID = {matchData.players[6].item_neutral}/>
                                            </div>
                                        </Table.Cell>

                                    </Table.Row>

                                    <Table.Row>

                                        <Table.Cell>
                                            <HeroPortrait heroID={matchData.players[7].hero_id} />
                                        </Table.Cell>

                                        <Table.Cell>
                                            {this.personaNameResolver(matchData.players[7].personaname, matchData.players[7].account_id)}
                                        </Table.Cell>

                                        <Table.Cell>{matchData.players[7].kills}</Table.Cell>
                                        <Table.Cell>{matchData.players[7].deaths}</Table.Cell>
                                        <Table.Cell>{matchData.players[7].assists}</Table.Cell>
                                        <Table.Cell>{matchData.players[7].gold}</Table.Cell>
                                        <Table.Cell>{matchData.players[7].net_worth}</Table.Cell>
                                        <Table.Cell>{matchData.players[7].last_hits} / {matchData.players[7].denies}</Table.Cell>
                                        <Table.Cell>{matchData.players[7].gold_per_min} / {matchData.players[7].xp_per_min}</Table.Cell>
                                        <Table.Cell>{matchData.players[7].hero_damage}</Table.Cell>
                                        <Table.Cell>{matchData.players[7].hero_healing}</Table.Cell>
                                        <Table.Cell>{matchData.players[7].tower_damage}</Table.Cell>

                                        <Table.Cell>
                                            <div style={itemTableCell}>
                                                <ItemPortrait itemID = {matchData.players[7].item_0}/>
                                                <ItemPortrait itemID = {matchData.players[7].item_1}/>
                                                <ItemPortrait itemID = {matchData.players[7].item_2}/>
                                                <ItemPortrait itemID = {matchData.players[7].item_3}/>
                                                <ItemPortrait itemID = {matchData.players[7].item_4}/>
                                                <ItemPortrait itemID = {matchData.players[7].item_5}/>
                                                <div style={verticalLine}></div>
                                                <ItemPortrait itemID = {matchData.players[7].backpack_0}/>
                                                <ItemPortrait itemID = {matchData.players[7].backpack_1}/>
                                                <ItemPortrait itemID = {matchData.players[7].backpack_2}/>
                                                <div style={verticalLine}></div>
                                                <ItemPortrait itemID = {matchData.players[7].item_neutral}/>
                                            </div>
                                        </Table.Cell>

                                    </Table.Row>

                                    <Table.Row>

                                        <Table.Cell>
                                            <HeroPortrait heroID={matchData.players[8].hero_id} />
                                        </Table.Cell>

                                        <Table.Cell>
                                            {this.personaNameResolver(matchData.players[8].personaname, matchData.players[8].account_id)}
                                        </Table.Cell>

                                        <Table.Cell>{matchData.players[8].kills}</Table.Cell>
                                        <Table.Cell>{matchData.players[8].deaths}</Table.Cell>
                                        <Table.Cell>{matchData.players[8].assists}</Table.Cell>
                                        <Table.Cell>{matchData.players[8].gold}</Table.Cell>
                                        <Table.Cell>{matchData.players[8].net_worth}</Table.Cell>
                                        <Table.Cell>{matchData.players[8].last_hits} / {matchData.players[8].denies}</Table.Cell>
                                        <Table.Cell>{matchData.players[8].gold_per_min} / {matchData.players[8].xp_per_min}</Table.Cell>
                                        <Table.Cell>{matchData.players[8].hero_damage}</Table.Cell>
                                        <Table.Cell>{matchData.players[8].hero_healing}</Table.Cell>
                                        <Table.Cell>{matchData.players[8].tower_damage}</Table.Cell>

                                        <Table.Cell>
                                            <div style={itemTableCell}>
                                                <ItemPortrait itemID = {matchData.players[8].item_0}/>
                                                <ItemPortrait itemID = {matchData.players[8].item_1}/>
                                                <ItemPortrait itemID = {matchData.players[8].item_2}/>
                                                <ItemPortrait itemID = {matchData.players[8].item_3}/>
                                                <ItemPortrait itemID = {matchData.players[8].item_4}/>
                                                <ItemPortrait itemID = {matchData.players[8].item_5}/>
                                                <div style={verticalLine}></div>
                                                <ItemPortrait itemID = {matchData.players[8].backpack_0}/>
                                                <ItemPortrait itemID = {matchData.players[8].backpack_1}/>
                                                <ItemPortrait itemID = {matchData.players[8].backpack_2}/>
                                                <div style={verticalLine}></div>
                                                <ItemPortrait itemID = {matchData.players[8].item_neutral}/>
                                            </div>
                                        </Table.Cell>

                                    </Table.Row>

                                    <Table.Row>

                                        <Table.Cell>
                                            <HeroPortrait heroID={matchData.players[9].hero_id} />
                                        </Table.Cell>

                                        <Table.Cell>
                                            {this.personaNameResolver(matchData.players[9].personaname, matchData.players[9].account_id)}
                                        </Table.Cell>

                                        <Table.Cell>{matchData.players[9].kills}</Table.Cell>
                                        <Table.Cell>{matchData.players[9].deaths}</Table.Cell>
                                        <Table.Cell>{matchData.players[9].assists}</Table.Cell>
                                        <Table.Cell>{matchData.players[9].gold}</Table.Cell>
                                        <Table.Cell>{matchData.players[9].net_worth}</Table.Cell>
                                        <Table.Cell>{matchData.players[9].last_hits} / {matchData.players[9].denies}</Table.Cell>
                                        <Table.Cell>{matchData.players[9].gold_per_min} / {matchData.players[9].xp_per_min}</Table.Cell>
                                        <Table.Cell>{matchData.players[9].hero_damage}</Table.Cell>
                                        <Table.Cell>{matchData.players[9].hero_healing}</Table.Cell>
                                        <Table.Cell>{matchData.players[9].tower_damage}</Table.Cell>

                                        <Table.Cell>
                                            <div style={itemTableCell}>
                                                <ItemPortrait itemID = {matchData.players[9].item_0}/>
                                                <ItemPortrait itemID = {matchData.players[9].item_1}/>
                                                <ItemPortrait itemID = {matchData.players[9].item_2}/>
                                                <ItemPortrait itemID = {matchData.players[9].item_3}/>
                                                <ItemPortrait itemID = {matchData.players[9].item_4}/>
                                                <ItemPortrait itemID = {matchData.players[9].item_5}/>
                                                <div style={verticalLine}></div>
                                                <ItemPortrait itemID = {matchData.players[9].backpack_0}/>
                                                <ItemPortrait itemID = {matchData.players[9].backpack_1}/>
                                                <ItemPortrait itemID = {matchData.players[9].backpack_2}/>
                                                <div style={verticalLine}></div>
                                                <ItemPortrait itemID = {matchData.players[9].item_neutral}/>
                                            </div>
                                        </Table.Cell>

                                    </Table.Row>
                                </Table.Body>
                            </Table>
                        </div>
                        
                        
                    </div>
    
                </div>}
                </div> 
            )
    }
}

const mapStateToProps = state => ({
    matchData: state.match.response,
    isLoading: state.match.loading
});


export default connect(mapStateToProps)(MatchViewPage);