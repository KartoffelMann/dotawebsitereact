import React, {Component} from 'react'
import { Link } from 'react-router-dom';
import { Button, Grid, Image } from 'semantic-ui-react';

import '../static/css/Header.css'

class Header extends Component {

    render() {
        return (
            <Grid centered columns={4} style={{ marginTop: '2px', marginBottom: '2px'}}>
                <Grid.Row stretched>
                    <Grid.Column width={3}>
                        <Image size='medium' as={Link} to="/" alt="dotaLogo" className="dotaLogo" src='https://cdn.cloudflare.steamstatic.com/apps/dota2/images/dota_react/global/dota2_logo_horiz.png'/>
                    </Grid.Column>

                    <Grid.Column  verticalAlign="middle">
                        <Button as={Link} to='/matches'> Recent Matches </Button>
                    </Grid.Column>

                    <Grid.Column  textAlign='right' verticalAlign="middle">
                        <Button as='a' href='https://dota2.com'> Play the game! </Button>
                    </Grid.Column>

                    <Grid.Column textAlign='right' verticalAlign="middle">
                        <Button as='a' href='https://www.twitch.tv/directory/game/Dota%202'> Watch Live! </Button>
                    </Grid.Column>

                    
                </Grid.Row>
                
            </Grid>
            
            
        )
        
    }
}

export default Header;