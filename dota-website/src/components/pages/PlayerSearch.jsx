import React, {Component} from 'react'
import { connect } from 'react-redux'
import { Grid } from 'semantic-ui-react'


import PlayerSearchComponent from './PlayerSearchComponent'

// For debugging
//import data from '../static/json/playerSearch.json'

import { getPlayerSearch } from '../../actions/playerActions'

class PlayerSearch extends Component {

    componentDidMount() {

        this.props.dispatch(getPlayerSearch(this.props.match.params.personaName))

    }

    render() {
        console.log(this)
        return (
            
            <div className="animate__animated animate_fadeIn animate__delay-1s">
                <div style={{textAlign: 'center'}}>
                    <h1>Search Results</h1>
                    <p>Results for '{this.props.match.params.personaName}'</p>
                </div>

                {!this.props.players.loading &&
                
                    <Grid celled container>
                        { Object.keys(this.props.players.response).map(key => {
                            return (
                                <PlayerSearchComponent history={this.props.history} key={key} data={this.props.players.response[key]}/>
                            )
                        })
                            
                        }
                    </Grid>
                }
                

            </div>
            

            
        )
    }
}

const mapStateToProps = state => ({
    players: state.player
})

export default connect(mapStateToProps)(PlayerSearch);