import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Image, Grid } from 'semantic-ui-react'
import moment from 'moment'

class PlayerSearchComponent extends Component {

    render() {
        const data = this.props.data;
        const date = moment(data.last_match_time);
        console.log(date)
        return (
            <Grid.Row className="animate__animated animate_fadeIn animate__delay-1s" columns={3}>
                <Grid.Column>
                    <Image src={data.avatarfull} href={`/players/${data.account_id}`}></Image>
                </Grid.Column>
                <Grid.Column>
                    <h2>{data.personaname}</h2>
                </Grid.Column>
                <Grid.Column>
                    <h2>Last Game Played: {date.fromNow()}</h2>
                </Grid.Column>
            </Grid.Row>
        )
    }
}

const mapStateToProps = state => ({
    players: state.player.response
})

export default connect(mapStateToProps)(PlayerSearchComponent);