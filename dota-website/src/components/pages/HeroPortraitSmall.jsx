import React, { Component } from 'react'
import heroList from '../static/json/dota_heroes.json'

// Makes a img tag and uses the hero list to get the correct image of the hero

class HeroPortraitSmall extends Component {

    constructor(props) {
        super(props);

        this.state = {
            heroID : this.props.heroID
        }
    }

    render() {
        let url = 'https://cdn.cloudflare.steamstatic.com/apps/dota2/images/dota_react/heroes/';
        let localizedName;


        heroList["heroes"].forEach(element => {
            if(element.id === this.state.heroID) {
                url += (element.name + ".png"); 
                localizedName = element.localized_name
            }
        });

        return (
            <div>
                <img style={{display: 'block', float: 'left', width: '20%', height: 'auto'}} src={url} alt={localizedName} title={localizedName}/>
            </div>
        )
    }
}

export default HeroPortraitSmall;