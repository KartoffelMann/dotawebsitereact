import React, { Component } from 'react'
import { Button, Grid, Input, Segment } from 'semantic-ui-react'

import '../static/css/HomePage.css'

class HomePage extends Component {

    constructor(props) {
        super(props)
        this.state = {
            matchID: "",
            playerName: "",
        }
    }

    onMatchIDSubmitClick = () => {
        this.props.history.push(`/match/${this.state.matchID}`)
        window.location.reload(false);
    };

    onPlayerIDSubmitClick = () => {
        this.props.history.push(`/search/${this.state.playerName}`)
        window.location.reload(false);
    };

    updateMatchID = e => {
        this.setState({ matchID : e.target.value })
    };

    updatePlayerID = e => {
        this.setState({ playerName : e.target.value })
    };

    resize = () => this.windowResizeListener()
    componentDidMount() {
        this.windowResizeListener()
        window.addEventListener('resize', this.resize)
        console.log(this.props)
    }

    componentWillUnmount() {
        window.removeEventListener('resize', this.resize)
    }

    windowResizeListener() {
        this.checkIfChangeInputPadding()
    }

    checkIfChangeInputPadding() {
        if(window.innerWidth - 50 < window.innerHeight) {
            document.getElementById('input').style.paddingTop = '400px'
        } else {
            document.getElementById('input').style.paddingTop = '680px'
        }
    }

    render() {

        const videoStyle = {
            width: '100%',
            position: 'absolute',
            
        }

        return (
            <div>
                <div className="animate__animated animate__fadeIn">
                    <video autoPlay={true}
                        style={videoStyle}
                        preload="auto" 
                        loop="loop" 
                        playsInline="">
                        <source type="video/mp4" src="https://cdn.cloudflare.steamstatic.com/apps/dota2/videos/dota_react/homepage/dota_montage_02.mp4"/>
                    </video>
                </div>


                <div id="input">
                    <Grid className="animate__animated animate__fadeIn animate__delay-1s">
                        <Grid.Row columns={2}>
                            <Grid.Column>
                                <div className="container">
                                    <Segment inverted>Match Data</Segment>
                                </div>
                                
                            </Grid.Column>

                            <Grid.Column>
                                <div className="container">
                                    <Segment inverted>Player Search</Segment>
                                </div>
                                
                            </Grid.Column>
                        </Grid.Row>
                        <Grid.Row columns={2}>
                            <Grid.Column>
                                <div className='container'>
                                    <Input onChange={this.updateMatchID} action={
                                        <Button onClick={this.onMatchIDSubmitClick}>Submit</Button>
                                    } placeholder='Input Match ID here...'></Input>
                                    
                                </div>
                            </Grid.Column>
                            <Grid.Column>
                                <div className='container'>
                                    
                                    <Input onChange={this.updatePlayerID} action={
                                        <Button onClick={this.onPlayerIDSubmitClick}>Submit</Button>
                                    } placeholder='Input Player Name here...'></Input>
                                </div>
                            </Grid.Column>
                        </Grid.Row>
                    </Grid>
                    
                </div>

                
            </div>
        )
    }
}

export default HomePage;