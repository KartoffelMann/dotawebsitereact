import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Button, Table } from 'semantic-ui-react'
import HeroPortraitSmall from './HeroPortraitSmall';

class RecentMatchesComponent extends Component {
    constructor(props) {
        super(props);
        this.onLinkClick = this.onLinkClick.bind(this);
    }

    returnSkillString(num) {
        switch(num) {
            case 1: 
                return "Normal Skill"
            case 2: 
                return "High Skill"
            case 3: 
                return "Very High Skill"
            default:
                return "Normal Skill"
        }
    }

    /**
     * Takes an integer value of seconds in the match, returns a formatted version MM:SS
     * @param {*} duration 
     * @returns Formatted String of match duration
     */
    returnDurationString(duration) {
        let minutes = duration / 60;

        let seconds = duration % 60;
        if(seconds < 10) {
            seconds = `0${seconds}`
        }

        return `${minutes | 0}:${seconds}`
    }

    returnMatchType(num) {
        switch(num) {
            case 0: {
                return "Unknown Game Mode"
            }
            case 1: {
                return "All Pick"
            }
            case 2: {
                return "Captains Mode"
            }
            case 3: {
                return "Random Draft"
            }
            case 4: {
                return "Single Draft"
            }
            case 5: {
                return "All Random"
            }
            case 6: {
                return "Intro"
            }
            case 7: {
                return "Reverse Captains Mode"
            }
            case 8: {
                return "Diretide"
            }
            case 9: {
                return "Greeviling"
            }
            case 10: {
                return "Tutorial"
            }
            case 11: {
                return "Mid Only"
            }
            case 12: {
                return "Least Played Heroes"
            }
            case 13: {
                return "Limited Heroes"
            }
            case 14: {
                return "Compendium Matchmaking"
            }
            case 15: {
                return "Custom Game"
            }
            case 16: {
                return "Captains Draft"
            }
            case 17: {
                return "Balanced Draft"
            }
            case 18: {
                return "Ability Draft"
            }
            case 19: {
                return "Event"
            }
            case 20: {
                return "All Random Deathmatch"
            }
            case 21: {
                return "1v1 Mid"
            }
            case 22: {
                return "All Pick"
            }
            case 23: {
                return "Turbo"
            }
            case 24: {
                return "Mutation"
            }
            default: {
                return "Unspecified"
            }
        }
    }

    // https://github.com/SteamDatabase/GameTracking-Dota2/blob/master/game/dota/pak01_dir/scripts/regions.txt
    returnRegionString(num) {
        switch(num) {
            case 1: return "US West"
            case 2: return "US East"
            case 3: return "Europe"
            case 5: return "Singapore"
            case 6: return "Dubai"
            case 12: return "Perfect World (China)"
            case 17: return "Perfect World (Guangdong)"
            case 18: return "Perfect World (Zhejiang)"
            case 20: return "Perfect World (Guangdong)"
            case 13: return "Perfect World (Unicom)"
            case 25: return "Perfect World (Unicom Tianjin)"
            case 8: return "Stockholm"
            case 10: return "Brazil"
            case 9: return "Austria"
            case 7: return "Australia"
            case 11: return "South Africa"
            case 14: return "Chile"
            case 15: return "Peru"
            case 16: return "India"
            case 19: return "Japan"
            case 37: return "Taiwan #1"
            default: return "Unknown"
        }
    }

    // https://github.com/odota/dotaconstants/blob/master/json/lobby_type.json
    returnLobbyType(num) {
        switch(num) {
            case 0: return "Normal Matchmaking"
            case 1: return "Practice"
            case 2: return "Tournament"
            case 3: return "Tutorial"
            case 4: return "Co-op Bots"
            case 5: return "Ranked Team Matchmaking"
            case 6: return "Ranked Solo Matchmaking"
            case 7: return "Ranked Matchmaking"
            case 8: return "1v1 Mid"
            case 9: return "Battle Cup"
            default: return "Unknown"
        }
    }

    returnWinnerElement(radiant_win) {
        if(radiant_win === true) {
            return <p style={{color: 'green'}}>Radiant Victory</p>;
        } else {
            return <p style={{color: 'red'}}>Dire Victory</p>;
        }
    }

    returnTeamCompPics(team) {
        let array = team.split(",")
        return (
            <div style={{whiteSpace: 'nowrap'}}>
                <HeroPortraitSmall heroID={parseInt(array[0])}/>
                <HeroPortraitSmall heroID={parseInt(array[1])}/>
                <HeroPortraitSmall heroID={parseInt(array[2])}/>
                <HeroPortraitSmall heroID={parseInt(array[3])}/>
                <HeroPortraitSmall heroID={parseInt(array[4])}/>
            </div>
        )
    }

    onLinkClick = (matchID) => {
        this.props.history.push(`/match/${matchID}`)
        window.location.reload(false);
    };

    render() {
        const matchData = this.props.data;
        return (
            <Table.Row>
                <Table.Cell>
                    <Button inverted onClick={() => this.onLinkClick(matchData.match_id)}>{matchData.match_id}</Button>
                </Table.Cell>
                <Table.Cell>
                    {this.returnMatchType(matchData.game_mode)}
                    <br></br>
                    {this.returnLobbyType(matchData.lobby_type)}
                </Table.Cell>
                <Table.Cell>
                    {this.returnWinnerElement(matchData.radiant_win)}
                </Table.Cell>
                <Table.Cell >
                    {this.returnDurationString(matchData.duration)}
                </Table.Cell>

                <Table.Cell>
                    {this.returnTeamCompPics(matchData.radiant_team)}
                </Table.Cell>
                    
                <Table.Cell>
                    {this.returnTeamCompPics(matchData.dire_team)}
                </Table.Cell>

            </Table.Row>
        )
    }
}

const mapStateToProps = state => ({
    matches: state.match.response
});


export default connect(mapStateToProps)(RecentMatchesComponent);